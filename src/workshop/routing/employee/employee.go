package employee

//Employee - Here we are exporting employee model
type Employee struct {
	ID          int
	Name        string
	Address     string
	Designation string
	Rating      float64
}
