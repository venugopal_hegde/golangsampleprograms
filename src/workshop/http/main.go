package main

import (
	"io"
	"net/http"
)

func hello(writer http.ResponseWriter, reader *http.Request) {
	io.WriteString(writer, "Helllo world")
}

func venu(writer http.ResponseWriter, reader *http.Request) {
	io.WriteString(writer, "venu")
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	mux.HandleFunc("/venu", venu)
	http.ListenAndServe(":3001", mux)
}
