package main

import (
	"io"
	"net/http"
)

func hello(writer http.ResponseWriter, reader *http.Request) {
	io.WriteString(writer, "Helllo world")
}

// func venu(writer http.ResponseWriter, reader *http.Request) {
// 	io.WriteString(writer, "venu")
// }

var mux map[string]func(w http.ResponseWriter, r *http.Request)

func main() {
	server := http.Server{
		Addr:    ":3001",
		Handler: &myHandler{},
	}
	mux = make(map[string]func(w http.ResponseWriter, r *http.Request))
	mux["/"] = hello

	server.ListenAndServe()
}

type myHandler struct {
}

func (*myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h, ok := mux[r.URL.String()]; ok {
		h(w, r)
		return
	}
}
