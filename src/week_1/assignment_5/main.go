package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var filePath string

func main() {
	mydir, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	filePath = mydir + `\tmp\dat\NOTES.txt`
	var a string
	for i := 1; i <= 100; i++ {
		if i == 1 {
			a = fmt.Sprint(i)
		} else {
			a = fmt.Sprint(a, "\n", i)
		}
	}
	writeFile([]byte(a))
	fmt.Println()
	fmt.Printf("File contents:\n%s", readFile())
}

func readFile() []byte {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatal(err)
	}
	return content
}

func writeFile(data []byte) {
	err := ioutil.WriteFile(filePath, data, 0644)
	if err != nil {
		log.Fatal(err)
	}
}
