package main

import "fmt"

func main() {
	var rows int
	fmt.Print("Please Enter the number of rows: ")
	fmt.Scanf("%d", &rows)
	for i := 1; i <= rows; i++ {
		for j := 1; j <= rows-i; j++ {
			fmt.Print(" ")
		}
		for j := i; j >= 1; j-- {
			fmt.Print(j)
		}
		for j := 2; j <= i; j++ {
			fmt.Print(j)
		}
		fmt.Println()
	}

}
