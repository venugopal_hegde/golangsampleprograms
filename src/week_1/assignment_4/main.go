package main

import "fmt"

func main() {
	fmt.Println("Helllo")
	var sum int = 0
	var avg int = 0
	array := []int{12, 56, 23, 67, 43, 23, 9}
	for _, v := range array {
		fmt.Printf("%d, ", v)
		sum += v
	}
	avg = sum / len(array)
	fmt.Println("sum = ", sum, "average = ", avg)
	fmt.Println("Swapped Array is :")
	swap(array)
	fmt.Println()
	fmt.Println("Sorted Array is:")
	sort(array)
}

func swap(array []int) {
	var temp int
	for i := 0; i < len(array)-1; i += 2 {
		temp = array[i]
		array[i] = array[i+1]
		array[i+1] = temp
	}
	for i := 0; i < len(array); i++ {
		fmt.Printf("%d, ", array[i])
	}
}

func sort(array []int) {
	var temp int
	for i := 0; i < len(array); i++ {
		for j := i + 1; j < len(array); j++ {
			if array[j] < array[i] {
				temp = array[j]
				array[j] = array[i]
				array[i] = temp
			}
		}
	}
	for i := 0; i < len(array); i++ {
		fmt.Printf("%d, ", array[i])
	}
}
