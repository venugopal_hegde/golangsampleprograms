package main

import (
	"fmt"
	"net/http"
)

type hotdog int

func (m hotdog) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Mcleod-Key", "this is form mcleod")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintln(w, "<h1> Any code goes here </h1>")
}

func main() {
	var d hotdog
	http.ListenAndServe(":3001", d)
}
