package main

import (
	"html/template"
	"io"
	"net/http"
)

type myHandler struct{}

var mux map[string]func(w http.ResponseWriter, r *http.Request)

// func addHandler(route string, handler http.HandlerFunc) {
// 	if _, ok := mux[route]; ok {
// 		panic("Route exists")
// 	}
// 	mux[route] = handler
// 	defer fmt.Println("Can reach", route)
// }

func (*myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h, ok := mux[r.URL.String()]; ok {
		h(w, r)
		return
	}
	route := r.URL.String()
	showContent(route, w, r)
}

func showContent(route string, w http.ResponseWriter, r *http.Request) {
	if route == "/" {
		home(w, r)
	} else if route == "/addcart" {
		addCart(w, r)
	} else {
		io.WriteString(w, "404 server not found")
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	homepage := template.New("homepage")
	parsedTemplate, _ := template.ParseFiles("homepage/home.html")
	if err := parsedTemplate.Execute(w, homepage); err != nil {
		panic(err)
	}
}

func addCart(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "item is added in cart")
}

func main() {
	server := http.Server{
		Addr:    ":9090",
		Handler: &myHandler{},
	}
	mux := make(map[string]func(w http.ResponseWriter, r *http.Request))
	mux["/"] = home
	mux["/addcart"] = addCart
	server.ListenAndServe()
}
