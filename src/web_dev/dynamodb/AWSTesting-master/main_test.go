package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// Define stub
type stubDynamoDB struct {
	dynamodbiface.DynamoDBAPI
}

type Record struct {
	ID   string
	URLs []string
}
type book struct {
	Title    string
	Year     int
	Author   string
	Category string
	Rating   float64
}

func (m *stubDynamoDB) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {

	items := []map[string]*dynamodb.AttributeValue{}

	mybook := book{
		"Hello",
		1997,
		"gopal",
		"Tec",
		4.5,
	}

	av, err := dynamodbattribute.MarshalMap(mybook)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	items = append(items, av)

	// val := dynamodb.AttributeValue{}
	// val.SetS("sample-val")
	// resp := make(map[string]*dynamodb.AttributeValue)
	// resp["key"] = &key
	// resp["val"] = &val

	// Returned canned response
	output := &dynamodb.GetItemOutput{
		Item: items[0],
	}
	return output, nil

}

// Sample Test Case

func TestDynamodb(t *testing.T) {
	svc := &stubDynamoDB{}
	res, err := callDynamodb(svc)
	if err != nil {
		t.Errorf("Error calling Dynamodb %d", err)
	}
	keyRes := *res.Item["Rating"].S
	valRes := *res.Item["Author"].S
	if keyRes != "sample-key" {
		t.Errorf("Wrong key returned. Shoule be sample-key, was %s", keyRes)
	}
	if valRes != "sample-val" {
		t.Errorf("Wrong value returned. Shoule be sample-val, was %s", valRes)
	}
}
