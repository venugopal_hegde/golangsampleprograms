package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

func main() {
	// Create Dynamodb AWS session
	sess, err := session.NewSession(&aws.Config{
		Endpoint:                      aws.String("http://localhost:8000"),
		Region:                        aws.String("eu-west-2"),
		CredentialsChainVerboseErrors: aws.Bool(true),
		Credentials:                   credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	svc := dynamodb.New(sess)

	res, err := callDynamodb(svc)
	if err != nil {
		fmt.Printf("Error returned %d", err)
	}
	fmt.Printf("Result %s", res.GoString())
}

func callDynamodb(svc dynamodbiface.DynamoDBAPI) (*dynamodb.GetItemOutput, error) {
	// Call GetItem
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("Books"),
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String("2000"),
			},
			"Title": {
				S: aws.String("Ruby"),
			},
		},
	})
	if err != nil {
		return nil, err
	}
	return result, nil
}
