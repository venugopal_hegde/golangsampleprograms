package models

type Item struct {
	Title string `json:"title"`
	Year  int    `json:"year"`
}
