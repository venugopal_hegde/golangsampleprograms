package models

type Movie struct {
	Year   int
	Title  string
	Plot   string
	Rating int
}
