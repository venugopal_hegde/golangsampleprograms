package crud

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func Update() {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:                      aws.String("http://localhost:8000"),
		Region:                        aws.String("eu-west-2"),
		CredentialsChainVerboseErrors: aws.Bool(true),
		Credentials:                   credentials.NewStaticCredentials("123", "123", ""),
	})
	svc := dynamodb.New(sess)

	tableName := "Movies"
	movieName := "The Big New Movie"
	movieYear := "2015"

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				N: aws.String("2017"),
			},
		},
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"year": {
				N: aws.String(movieYear),
			},
			"title": {
				S: aws.String(movieName),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set Year = :r"),
	}

	_, err1 := svc.UpdateItem(input)
	if err1 != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Successfully updated ", movieName, movieYear)
}
