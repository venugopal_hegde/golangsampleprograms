package main

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

// Use struct tags much like the standard JSON library,
// you can embed anonymous structs too!
type widget struct {
	UserID int       // Hash key, a.k.a. partition key
	Time   time.Time // Range key, a.k.a. sort key

	Msg       string              `dynamo:"Message"`
	Count     int                 `dynamo:",omitempty"`
	Friends   []string            `dynamo:",set"`     // Sets
	Set       map[string]struct{} `dynamo:",set"`     // Map sets, too!
	SecretKey string              `dynamo:"-"`        // Ignored
	Category  string              `dynamo:"Category"` // Global Secondary Index
}

func main() {

	// cfg := aws.Config{
	// 	CredentialsChainVerboseErrors: aws.Bool(false),
	// }
	// cfg.Region = aws.String("eu-west-2")
	// endpoint := "http://localhost:8000"
	// if endpoint != "" {
	// 	cfg.Endpoint = aws.String(endpoint)
	// }
	// //WithCredentialsChainVerboseErrors = aws.Bool(true)
	// creds := credentials.NewStaticCredentials(" ", " ", "")
	// cfg.Credentials = creds
	// db := dynamo.New(session.New(), &cfg)
	// wid := widget{}
	// tble := db.CreateTable("widgets", wid).Run()+
	// fmt.Println(db.ListTables().All())
	// fmt.Println(tble)
	// table := db.Table("Widgets")

	// // put item
	// w := widget{UserID: 613, Time: time.Now(), Msg: "hello"}
	// err := table.Put(w).Run()

	// // get the same item
	// var result widget
	// err = table.Get("UserID", w.UserID).
	// 	Range("Time", dynamo.Equal, w.Time).
	// 	Filter("'Count' = ? AND $ = ?", w.Count, "Message", w.Msg). // placeholders in expressions
	// 	One(&result)

	// // get by index
	// err = table.Get("Cateogry", "hoge").
	// 	Index("category-index").
	// 	One(&result)

	// // get all items
	// var results []widget
	// err = table.Scan().All(&results)

	// if err != nil {
	// 	fmt.Println(err)
	// }
	table := widget{}

	cfg := aws.Config{
		Endpoint:                      aws.String("http://localhost:8000"),
		Region:                        aws.String("eu-west-2"),
		CredentialsChainVerboseErrors: aws.Bool(true),
	}
	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &cfg)
	//tableName := xid.New().String()
	tableName := aws.String("venugopal")
	err := db.CreateTable(tableName, table).Run()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(tableName)
}
