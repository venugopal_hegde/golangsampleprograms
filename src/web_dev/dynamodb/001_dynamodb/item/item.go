package item

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
	"github.com/rs/xid"
)

type Item struct {
	Id   string `dynamo:"item_id,hash"`
	Name string `dynamo:"name"`
}

type ItemService struct {
	itemTable dynamo.Table
}

type Foo interface {
	CreateItem(item *Item) error
	GetItem(item *Item)
}

func newDynamoTable(tableName, endpoint string) (dynamo.Table, error) {
	if tableName == "" {
		return dynamo.Table{}, fmt.Errorf("you must supply a table name")
	}
	cfg := aws.Config{}
	cfg.Region = aws.String("eu-west-2")
	if endpoint != "" {
		cfg.Endpoint = aws.String(endpoint)
	}
	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &cfg)
	table := db.Table(tableName)
	return table, nil
}

func NewItemService(itemTableName string) (*ItemService, error) {
	dynamoTable, err := newDynamoTable(itemTableName, "Db")
	if err != nil {
		return nil, err
	}
	return &ItemService{
		itemTable: dynamoTable,
	}, nil
}

func (i *ItemService) CreateItem(item *Item) error {
	item.Id = xid.New().String()
	return i.itemTable.Put(item).Run()
}

func (i *ItemService) GetItem(item *Item) error {
	return i.itemTable.Get("item_id", item.Id).One(item)
}
