package main

import (
	"fmt"
	"web_dev/dynamodb/001_dynamodb/item"

	"github.com/guregu/dynamo"
	"github.com/rs/xid"
)

func main() {
	createdTable, err := item.NewItemService("Employee")
	if err != nil {
		fmt.Println("Errrrrro")
	}
	item1 := item.Item{
		"escf",
		"jjjj",
	}
	item1.Id = xid.New().String()
	dynamo.Table.Put(dynamo.Table{}, item1).Run()

	fmt.Println(createdTable)
}
