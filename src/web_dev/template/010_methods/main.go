package main

import (
	"log"
	"os"
	"text/template"
)

type person struct {
	Name string
	Age  int
}

func (p person) SomeProcessing() int {
	return 7
}

func (p person) AgeDbl() int {
	return p.Age * 2
}

func (p person) TakesArg(x int) int {
	return x * 2
}

func main() {
	//sages := []string{"Gandhi", "Milk", "Buddha", "Jesus"}

	p := person{
		Name: "Buddha",
		Age:  56,
	}
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.ExecuteTemplate(os.Stdout, "index.gohtml", p)
	if err != nil {
		log.Fatalln(err)
	}
}
