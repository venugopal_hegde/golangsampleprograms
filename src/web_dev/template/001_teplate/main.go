package main

import (
	"log"
	"os"
	"text/template"
)

func main(){
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln(err)
	}
}