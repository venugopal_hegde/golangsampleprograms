package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.ExecuteTemplate(os.Stdout, "index.gohtml", `Release self-focus: embrace other-focus`)
	if err != nil {
		log.Fatalln(err)
	}
}
