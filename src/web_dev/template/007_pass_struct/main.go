package main

import (
	"log"
	"os"
	"strings"
	"text/template"
)

var fm = template.FuncMap{
	"uc": strings.ToUpper,
	"ft": firstThree,
}

func firstThree(s string) string {
	s = strings.TrimSpace(s)
	s = s[:3]
	return s
}

type sage struct {
	Name  string
	Motto string
}

func main() {
	//sages := []string{"Gandhi", "Milk", "Buddha", "Jesus"}

	buddha := sage{
		Name:  "Buddha",
		Motto: "The belief of no beliefs",
	}
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.ExecuteTemplate(os.Stdout, "index.gohtml", buddha)
	if err != nil {
		log.Fatalln(err)
	}
}
