package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	//sages := []string{"Gandhi", "Milk", "Buddha", "Jesus"}

	sages := map[string]string{
		"India":    "Gandhi",
		"America":  "MLK",
		"Meditate": "Buddha",
		"Love":     "Jesus",
	}
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.ExecuteTemplate(os.Stdout, "index.gohtml", sages)
	if err != nil {
		log.Fatalln(err)
	}
}
