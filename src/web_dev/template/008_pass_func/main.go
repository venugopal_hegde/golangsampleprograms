package main

import (
	"log"
	"os"
	"text/template"
)

type sage struct {
	Name  string
	Motto string
}

func main() {
	//sages := []string{"Gandhi", "Milk", "Buddha", "Jesus"}

	buddha := sage{
		Name:  "Buddha",
		Motto: "The belief of no beliefs",
	}
	tpcl, err := template.ParseFiles("index.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpcl.ExecuteTemplate(os.Stdout, "index.gohtml", buddha)
	if err != nil {
		log.Fatalln(err)
	}
}
