package main

import (
	"net/http"
	"web_dev/organised/004_controllers/controllers"

	"github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController()
	r.GET("/", uc.Index)
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	http.ListenAndServe("localhost:3001", r)
}
