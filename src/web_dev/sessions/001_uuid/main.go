package main

import (
	"text/template"

	"net/http"

	uuid "github.com/satori/go.uuid"
)

type user struct {
	UserName string
	First    string
	Last     string
}

var tpl *template.Template
var dbUsers = map[string]user{}
var dbSesisons = map[string]string{}

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/bar", bar)
	http.ListenAndServe(":3001", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("session-id")
	if err != nil {
		id, _ := uuid.NewV4()
		c = &http.Cookie{
			Name:     "session-id",
			Value:    id.String(),
			HttpOnly: true,
		}
		http.SetCookie(w, c)
	}
	var u user
	if un, ok := dbSesisons[c.Value]; ok {
		u = dbUsers[un]
	}

	if req.Method == http.MethodPost {
		un := req.FormValue("username")
		f := req.FormValue("firstname")
		l := req.FormValue("lastname")
		u = user{un, f, l}
		dbSesisons[c.Value] = un
		dbUsers[un] = u
	}
	tpl.ExecuteTemplate(w, "index.gohtml", u)
}

func bar(w http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("session-id")
	if err != nil {
		http.Redirect(w, req, "/", http.StatusSeeOther)
	}
	un, ok := dbSesisons[c.Value]
	if !ok {
		http.Redirect(w, req, "/", http.StatusSeeOther)
	}
	u := dbUsers[un]
	tpl.ExecuteTemplate(w, "bar.gohtml", u)
}
