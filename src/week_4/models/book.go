package models

type Book struct {
	Title    string
	Year     int
	Author   string
	Category string
	Rating   float64
}
