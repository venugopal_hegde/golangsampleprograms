package models

type Update struct {
	Title    string
	Year     string
	Author   string
	Category string
	Rating   string
}
