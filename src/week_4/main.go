package main

import (
	"net/http"
	"week_4/controllers"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func main() {
	bc := controllers.NewBookController(getSession())
	http.HandleFunc("/create", bc.CreateBook)
	http.HandleFunc("/find", bc.GetBook)
	http.HandleFunc("/delete", bc.DeleteBook)
	http.HandleFunc("/update", bc.UpdateBook)
	http.HandleFunc("/getall", bc.GetAllBooks)
	http.ListenAndServe(":3002", nil)
}

func getSession() *session.Session {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:                      aws.String("http://localhost:8000"),
		Region:                        aws.String("eu-west-2"),
		CredentialsChainVerboseErrors: aws.Bool(true),
		Credentials:                   credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	return sess
}
