package repo

import (
	"fmt"
	"os"
	"week_4/models"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type BookRepo struct {
	dynamodbiface.DynamoDBAPI
}

func NewBookRepo(s dynamodbiface.DynamoDBAPI) *BookRepo {
	return &BookRepo{s}
}

func (br BookRepo) GetAllBooks() []models.Book {
	tableName := "Books"
	proj := expression.NamesList(expression.Name("Title"), expression.Name("Year"), expression.Name("Rating"), expression.Name("Author"), expression.Name("Category"))
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}
	result, err := br.Scan(params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
		os.Exit(1)
	}

	items := []models.Book{}

	for _, i := range result.Items {
		item := models.Book{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			fmt.Println("Got error unmarshalling:")
			fmt.Println(err.Error())
			os.Exit(1)
		}
		items = append(items, item)
	}
	return items
}

func (br BookRepo) InsertBook(book models.Book) bool {
	av, err := dynamodbattribute.MarshalMap(book)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	tableName := "Books"

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	result, err1 := br.PutItem(input)
	if err1 != nil {
		fmt.Println("Got error calling PutItem:")
		fmt.Println(err1.Error())
		os.Exit(1)
	}
	fmt.Print(result)
	return true
}

func (br BookRepo) UpdateBook(book models.Update) bool {
	tableName := "Books"
	bookName := book.Title
	bookYear := book.Year
	bookRating := book.Rating
	bookCategory := book.Category
	bookAuthor := book.Author

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				N: aws.String(bookRating),
			},
			":c": {
				S: aws.String(bookCategory),
			},
			":a": {
				S: aws.String(bookAuthor),
			},
		},
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(bookYear),
			},
			"Title": {
				S: aws.String(bookName),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set Rating = :r , Category = :c, Author = :a"),
	}

	_, err := br.UpdateItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}
	return true
}

func (br BookRepo) DeleteBook(find models.Find) bool {

	tableName := "Books"
	movieName := find.Title
	movieYear := find.Year

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(movieYear),
			},
			"Title": {
				S: aws.String(movieName),
			},
		},
		TableName: aws.String(tableName),
	}

	_, err := br.DeleteItem(input)
	if err != nil {
		fmt.Println("Got error calling DeleteItem")
		fmt.Println(err.Error())
		return false
	}
	return true
}

func (br BookRepo) GetBook(find models.Find) models.Book {

	tableName := "Books"
	bookName := find.Title
	bookYear := find.Year

	result, err := br.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(bookYear),
			},
			"Title": {
				S: aws.String(bookName),
			},
		},
	})
	if err != nil {
		fmt.Println(err.Error())
	}
	book := models.Book{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &book)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if book.Title == "" {
		fmt.Println("Could not find")
	}

	return book
}
