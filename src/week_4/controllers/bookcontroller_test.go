package controllers

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"week_4/models"
	"week_4/repo"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

type stubDynamoDB struct {
	dynamodbiface.DynamoDBAPI
}

func (m *stubDynamoDB) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {

	items := []map[string]*dynamodb.AttributeValue{}
	mybook := models.Book{
		"Ruby",
		2000,
		"gopal",
		"TecFoo123",
		9,
	}

	av, err := dynamodbattribute.MarshalMap(mybook)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	items = append(items, av)

	output := &dynamodb.GetItemOutput{
		Item: items[0],
	}

	return output, nil
}

func (m *stubDynamoDB) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {

	output := &dynamodb.PutItemOutput{}
	return output, nil

}

func (m *stubDynamoDB) Scan(*dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {

	items := []map[string]*dynamodb.AttributeValue{}
	mybook := models.Book{
		"Ruby",
		2000,
		"gopal",
		"TecFoo123",
		9,
	}
	mybook1 := models.Book{
		"Ruby1",
		3000,
		"gopal",
		"TecFoo123",
		9,
	}
	mybook2 := models.Book{
		"Ruby2",
		4000,
		"gopal",
		"TecFoo123",
		9,
	}

	av, err := dynamodbattribute.MarshalMap(mybook)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	av1, err := dynamodbattribute.MarshalMap(mybook1)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	av2, err := dynamodbattribute.MarshalMap(mybook2)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	items = append(items, av)
	items = append(items, av1)
	items = append(items, av2)

	output := &dynamodb.ScanOutput{
		Items: items,
	}
	return output, nil
}
func TestGetAllBook(t *testing.T) {
	svc := &stubDynamoDB{}
	br := repo.NewBookRepo(svc)
	books := br.GetAllBooks()
	fmt.Print(books)
}

func TestMyInsertBook(t *testing.T) {
	svc := &stubDynamoDB{}
	br := repo.NewBookRepo(svc)
	book := models.Book{
		"Magic",
		8709,
		"hegde",
		"comedy",
		9.0,
	}
	result := br.InsertBook(book)
	fmt.Print(result)

}

func TestGetBook(t *testing.T) {
	svc := &stubDynamoDB{}
	br := repo.NewBookRepo(svc)
	find := models.Find{
		"Ruby",
		"2000",
	}
	book := br.GetBook(find)
	fmt.Println(book)
	// req, err := http.NewRequest("GET", "/find", nil)
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// q := req.URL.Query()
	// q.Add("title", "Python")
	// q.Add("year", "2025")
	// req.URL.RawQuery = q.Encode()
	// rr := httptest.NewRecorder()
	// bc := NewBookController(getSession())
	// handler := http.HandlerFunc(bc.GetBook)
	// handler.ServeHTTP(rr, req)
	// if status := rr.Code; status != http.StatusOK {
	// 	t.Errorf("handler returned wrong status code: got %v want %v",
	// 		status, http.StatusOK)
	// }
	// expected := `{"Title":"Python","Year":2025,"Author":"gopal","Category":"TecFoo1234","Rating":4.8}`

	// if strings.Compare(rr.Body.String(), expected) != 1 {
	// 	t.Errorf("handler returned unexpected body: got %v want %v",
	// 		rr.Body.String(), expected)
	// }

}

func TestCreateBook(t *testing.T) {
	var jsonStr = []byte(`{"Title":"Ruby","Year":2001,"Author":"gopal","Category":"TecFoo1234","Rating":4.0}`)

	req, err := http.NewRequest("POST", "/create", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	bc := NewBookController(getSession())
	handler := http.HandlerFunc(bc.CreateBook)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"Title":"Ruby","Year":2001,"Author":"gopal","Category":"TecFoo1234","Rating":4.0}`
	if strings.Compare(rr.Body.String(), expected) != 1 {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestUpdateBook(t *testing.T) {
	var jsonStr = []byte(`{"Title":"Ruby","Year":"2000","Author":"gopal","Category":"TecFoo123","Rating":"9.0"}`)

	req, err := http.NewRequest("PUT", "/update", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	bc := NewBookController(getSession())
	handler := http.HandlerFunc(bc.UpdateBook)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"Title":"Ruby","Year":"2000","Author":"gopal","Category":"TecFoo123","Rating":"9.0"}`
	if strings.Compare(rr.Body.String(), expected) != 1 {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestDeleteBook(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/delete", nil)
	if err != nil {
		t.Fatal(err)
	}
	q := req.URL.Query()
	q.Add("title", "Python")
	q.Add("year", "2025")
	req.URL.RawQuery = q.Encode()
	rr := httptest.NewRecorder()
	bc := NewBookController(getSession())
	handler := http.HandlerFunc(bc.DeleteBook)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := "Deleted successfully"
	if strings.Compare(rr.Body.String(), expected) != 1 {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func getSession() *session.Session {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:                      aws.String("http://localhost:8000"),
		Region:                        aws.String("eu-west-2"),
		CredentialsChainVerboseErrors: aws.Bool(true),
		Credentials:                   credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	return sess
}
