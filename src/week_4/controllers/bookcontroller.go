package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"week_4/models"
	"week_4/repo"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type BookController struct {
	*session.Session
}

func NewBookController(s *session.Session) *BookController {
	return &BookController{s}
}

func (bc BookController) CreateBook(w http.ResponseWriter, r *http.Request) {
	book := models.Book{}
	json.NewDecoder(r.Body).Decode(&book)
	bookrepo := repo.NewBookRepo(dynamodb.New(bc.Session))
	if bookrepo.InsertBook(book) == true {
		uj, err := json.Marshal(book)
		if err != nil {
			fmt.Println(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK) // 200
		fmt.Fprintf(w, "%s\n", uj)
		return
	}
	fmt.Fprint(w, "Failed to insert")
}

func (bc BookController) GetBook(w http.ResponseWriter, r *http.Request) {
	find := models.Find{}
	find.Title = r.FormValue("title")
	find.Year = r.FormValue("year")
	bookrepo := repo.NewBookRepo(dynamodb.New(bc.Session))
	book := bookrepo.GetBook(find)
	uj, err := json.Marshal(book)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}

func (bc BookController) DeleteBook(w http.ResponseWriter, r *http.Request) {

	find := models.Find{}
	find.Title = r.FormValue("title")
	find.Year = r.FormValue("year")

	bookrepo := repo.NewBookRepo(dynamodb.New(bc.Session))
	if bookrepo.DeleteBook(find) == true {
		fmt.Fprint(w, "Deleted successfully")
		return
	}

	fmt.Fprint(w, "Deletion failed")
}

func (bc BookController) UpdateBook(w http.ResponseWriter, r *http.Request) {
	book := models.Update{}
	json.NewDecoder(r.Body).Decode(&book)
	bookrepo := repo.NewBookRepo(dynamodb.New(bc.Session))
	if bookrepo.UpdateBook(book) == true {
		uj, err := json.Marshal(book)
		if err != nil {
			fmt.Println(err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK) // 200
		fmt.Fprintf(w, "%s\n", uj)
		return
	}
	fmt.Fprint(w, "Failed to update")
}

func (bc BookController) GetAllBooks(w http.ResponseWriter, r *http.Request) {

	bookrepo := repo.NewBookRepo(dynamodb.New(bc.Session))
	books := bookrepo.GetAllBooks()
	uj, err := json.Marshal(books)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}
