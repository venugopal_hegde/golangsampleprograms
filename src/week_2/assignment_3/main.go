package main

import "fmt"

type calculate struct {
	a int
	b int
}

func (calc calculate) add() {
	fmt.Println("Addition = ", calc.a+calc.b)
}

func (calc calculate) subtract() {
	fmt.Println("Subtraction = ", calc.a-calc.b)
}

func (calc calculate) multiply() {
	fmt.Println("Multiplication = ", calc.a*calc.b)
}

func (calc calculate) devide() {
	fmt.Println("Division = ", calc.a/calc.b)
}

type calci interface {
	add()
	subtract()
	multiply()
	devide()
}

func calcuteTwoInt(cal calci) {
	cal.add()
	cal.subtract()
	cal.multiply()
	cal.devide()
}
func main() {
	inputs := calculate{
		20,
		5,
	}
	calcuteTwoInt(inputs)
}
