package main

import "fmt"

type employee struct {
	ID        int
	firstname string
	lastname  string
	email     string
	phone     int
	salary    int
}

var employees []employee

func main() {
	addEmployee()
	getEmployees()
}

func addEmployee() {
	var numberOfEmployee int
	var newEmployee employee
	fmt.Println("Enter the number of employees you want to insert :")
	fmt.Scanf("%d", &numberOfEmployee)
	fmt.Println("Please Enter employee details one by one")
	for i := 0; i < numberOfEmployee; i++ {
		fmt.Printf("Enter the details of employee %d\n", i+1)
		fmt.Scanf("%s")
		fmt.Print("Enter first name : ")
		fmt.Scanf("%s", &newEmployee.firstname)
		fmt.Scanf("%s")
		fmt.Print("Enter last name : ")
		fmt.Scanf("%s", &newEmployee.lastname)
		fmt.Scanf("%s")
		fmt.Print("Enter email : ")
		fmt.Scanf("%s", &newEmployee.email)
		fmt.Scanf("%d")
		fmt.Print("Enter phone number : ")
		fmt.Scanf("%d", &newEmployee.phone)
		fmt.Scanf("%d")
		fmt.Print("Enter salary : ")
		fmt.Scanf("%d", &newEmployee.salary)
		fmt.Println()
		if i == 0 {
			newEmployee.ID = 1
		} else {
			newEmployee.ID = employees[len(employees)-1].ID + 1
		}
		employees = append(employees, newEmployee)
	}
}

func getEmployees() {
	for _, v := range employees {
		fmt.Println(v.ID, v.firstname, v.lastname, v.email, v.phone, v.salary)
	}
}
