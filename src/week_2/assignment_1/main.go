package main

import "fmt"

type employee struct {
	ID        int
	firstname string
	lastname  string
	email     string
	phone     int
	salary    int
}

func main() {
	employee1 := employee{
		ID:        1,
		firstname: "venu",
		lastname:  "hegde",
		email:     "venuhegde6@gmail.com",
		phone:     9632968298,
		salary:    2323,
	}

	employee2 := employee{
		ID:        2,
		firstname: "gopl",
		lastname:  "hegde",
		email:     "venuhegde6@gmail.com",
		phone:     9632968338,
		salary:    2323,
	}

	employee3 := employee{
		ID:        3,
		firstname: "veeresh",
		lastname:  "gankar",
		email:     "ganka36@gmail.com",
		phone:     9644968298,
		salary:    2323,
	}

	employee4 := employee{
		ID:        4,
		firstname: "rahul",
		lastname:  "naik",
		email:     "nail43@gmail.com",
		phone:     96448298,
		salary:    2323,
	}

	fmt.Println(employee1.ID, employee1.firstname, employee1.lastname, employee1.email, employee1.phone, employee1.salary)
	fmt.Println(employee2.ID, employee2.firstname, employee2.lastname, employee2.email, employee2.phone, employee2.salary)
	fmt.Println(employee3.ID, employee3.firstname, employee3.lastname, employee3.email, employee3.phone, employee3.salary)
	fmt.Println(employee4.ID, employee4.firstname, employee4.lastname, employee4.email, employee4.phone, employee4.salary)
}
