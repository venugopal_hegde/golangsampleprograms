package models

import "gopkg.in/mgo.v2/bson"

type CheckoutData struct {
	Id         bson.ObjectId `json:"id" bson:"_id"`
	Products   string        `json:"products" bson:"products"`
	TotalPrice string        `json:"totalPrice" bson:"totalPrice"`
}