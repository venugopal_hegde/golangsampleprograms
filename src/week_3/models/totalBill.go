package models

type TotalBill struct {
	BilledProduct []Bill
	TotalPrice    float64
}