package models

import "gopkg.in/mgo.v2/bson"

//User Model
type Product struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Price       float64       `json:"price" bson:"price"`
	Category    string        `json:"category" bson:"category"`
	Image       string        `json:"image" bson:"image"`
	Description string        `json:"description" bson:"description"`
	Quantity    int64         `json:"quantity" bson:"quantity"`
}
