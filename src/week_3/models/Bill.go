package models

type Bill struct {
	Product Product
	Count   string
	Price   float64
}
