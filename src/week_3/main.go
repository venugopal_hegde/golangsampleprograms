package main

import (
	"net/http"
	"week_3/controllers"

	"gopkg.in/mgo.v2"
)

func main() {
	pc := controllers.NewProductController(getSession())
	http.HandleFunc("/", pc.GetProducts)
	http.HandleFunc("/create", pc.CreateProducts)
	http.HandleFunc("/cart", pc.MyCart)
	http.HandleFunc("/updateCart", pc.UpdateCartCookie)
	http.HandleFunc("/bill", pc.GetBill)
	http.HandleFunc("/checkout", pc.Checkout)
	http.HandleFunc("/allbills", pc.GetAllMyBills)
	http.HandleFunc("/update", pc.Update)
	http.HandleFunc("/find", pc.FindProduct)
	http.HandleFunc("/delete", pc.DeleteProdudct)
	http.Handle("/public/", http.StripPrefix("/public", http.FileServer(http.Dir("./public"))))
	http.ListenAndServe(":3001", nil)
}

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://172.17.0.2:27017")

	if err != nil {
		panic(err)
	}
	return s
}
