package repo

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"week_3/models"
)

type ProductRepo struct{}

func NewProductRepo() *ProductRepo {
	return &ProductRepo{}
}

func (productRepo ProductRepo) GetHexFromString(id string) string {
	trim1 := strings.Trim(id, "ObjectIdHex")
	trim2 := strings.Trim(trim1, ")")
	trim3 := strings.Trim(trim2, "(")
	trim4 := strings.Split(trim3, "\"")
	return trim4[1]
}

func (productRepo ProductRepo) GetCookie(w http.ResponseWriter, req *http.Request) *http.Cookie {
	c, err := req.Cookie("session")
	if err != nil {
		//sID, _ := uuid.NewV4()
		c = &http.Cookie{
			Name:  "session",
			Value: "",
		}
		http.SetCookie(w, c)
	}
	return c
}

func (productRepo ProductRepo) AppendValue(w http.ResponseWriter, c *http.Cookie, id string, count string) *http.Cookie {
	s := c.Value
	data, _ := base64.StdEncoding.DecodeString(s)
	isExist := false
	var val []models.CookieModel
	var newCookie = models.CookieModel{
		id,
		count,
	}
	json.Unmarshal(data, &val)
	for i, v := range val {
		if v.Id == id {
			val[i].Count = count
			isExist = true
		}
	}
	if !isExist {
		val = append(val, newCookie)
	}
	pj, err := json.Marshal(val)
	if err != nil {
		fmt.Println(err)
	}
	c.Value = base64.StdEncoding.EncodeToString(pj)
	http.SetCookie(w, c)
	return c
}
