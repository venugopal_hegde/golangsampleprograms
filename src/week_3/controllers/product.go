package controllers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"week_3/models"
	"week_3/repo"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName                = "go-ecommerce"
	productCollectionName = "products"
	billCollectionName    = "bills"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

type ProductController struct {
	session *mgo.Session
}

func NewProductController(s *mgo.Session) *ProductController {
	return &ProductController{s}
}

var productRepo = repo.NewProductRepo()

func (pc ProductController) GetProducts(w http.ResponseWriter, r *http.Request) {
	var productList []models.Product
	err := pc.session.DB(dbName).C(productCollectionName).Find(nil).All(&productList)
	if err != nil {
		fmt.Fprint(w, "Error Fetching Data")
		return
	}
	tpl.ExecuteTemplate(w, "index.gohtml", productList)
}

func (pc ProductController) CreateProducts(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		price, err := strconv.ParseFloat(r.FormValue("price"), 64)
		if err != nil {
			fmt.Fprint(w, "Unable to convert to Float")
			return
		}
		quantity, err := strconv.ParseInt(r.FormValue("quantity"), 10, 64)
		if err != nil {
			fmt.Fprint(w, "Unable to convert to Int")
			return
		}
		newProduct := models.Product{
			bson.NewObjectId(),
			r.FormValue("name"),
			price,
			r.FormValue("category"),
			r.FormValue("image"),
			r.FormValue("description"),
			quantity,
		}
		if err := pc.session.DB(dbName).C(productCollectionName).Insert(newProduct); err != nil {
			fmt.Fprint(w, "Error While Inserting Product")
			return
		}
		http.Redirect(w, r, "/", 302)
	}
	tpl.ExecuteTemplate(w, "addproduct.gohtml", nil)
}

func (pc ProductController) Update(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		id := r.FormValue("id")
		price, err := strconv.ParseFloat(r.FormValue("price"), 64)
		if err != nil {
			fmt.Fprint(w, "Unable to convert to Float")
			return
		}
		quantity, err := strconv.ParseInt(r.FormValue("quantity"), 10, 64)
		if err != nil {
			fmt.Fprint(w, "Unable to convert to Int")
		}

		updatedProduct := models.Product{
			bson.ObjectIdHex(productRepo.GetHexFromString(id)),
			r.FormValue("name"),
			price,
			r.FormValue("category"),
			r.FormValue("image"),
			r.FormValue("description"),
			quantity,
		}
		if err := pc.session.DB(dbName).C(productCollectionName).UpdateId(updatedProduct.Id, &updatedProduct); err != nil {
			fmt.Fprint(w, "Unable to update data")
			return
		}
		http.Redirect(w, r, "/", 302)
		return
	}
	id := r.FormValue("id")
	product := models.Product{}
	if err := pc.session.DB(dbName).C(productCollectionName).FindId(bson.ObjectIdHex(productRepo.GetHexFromString(id))).One(&product); err != nil {
		fmt.Fprint(w, "Unable to find the ID")
		return
	}
	tpl.ExecuteTemplate(w, "addproduct.gohtml", product)

}

func (pc ProductController) DeleteProdudct(w http.ResponseWriter, r *http.Request) {

	id := r.FormValue("id")

	// Delete user
	if err := pc.session.DB(dbName).C(productCollectionName).RemoveId(bson.ObjectIdHex(productRepo.GetHexFromString(id))); err != nil {
		w.WriteHeader(404)
		return
	}

	http.Redirect(w, r, "/", 302)
}

func (pc ProductController) FindProduct(w http.ResponseWriter, r *http.Request) {
	search := r.FormValue("search")
	products := []models.Product{}
	if err := pc.session.DB(dbName).C(productCollectionName).Find(bson.M{"name": bson.RegEx{search + "*", ""}}).All(&products); err != nil {
		fmt.Fprint(w, "Unable to find the ID")
		return
	}
	tpl.ExecuteTemplate(w, "detail.gohtml", products)

}

func (pc ProductController) MyCart(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session")
	if err != nil {
		fmt.Fprintf(w, "No data exists inside the cart")
		return
	}
	cookieValue, err := base64.StdEncoding.DecodeString(cookie.Value)
	if err != nil {
		fmt.Fprint(w, "Unable to decode cookie data")
		return
	}
	var cookieModels []models.CookieModel
	var cartList []models.Cart
	json.Unmarshal(cookieValue, &cookieModels)

	for _, v := range cookieModels {
		product := models.Product{}
		if err := pc.session.DB(dbName).C(productCollectionName).FindId(bson.ObjectIdHex(v.Id)).One(&product); err != nil {
			fmt.Fprint(w, "Unable to find the given ID data does'nt exists")
			return
		}
		cart := models.Cart{
			product,
			v.Count,
		}
		cartList = append(cartList, cart)
	}
	tpl.ExecuteTemplate(w, "cart.gohtml", cartList)
}

var bill = models.Bill{}

func (pc ProductController) GetBill(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session")
	if err != nil {
		fmt.Fprint(w, "No data exists in cookie")
		return
	}
	cookieValue, _ := base64.StdEncoding.DecodeString(cookie.Value)
	var cookieValList []models.CookieModel
	var billsList []models.Bill
	json.Unmarshal(cookieValue, &cookieValList)
	sum := 0.0
	for _, v := range cookieValList {
		product := models.Product{}
		if err := pc.session.DB(dbName).C(productCollectionName).FindId(bson.ObjectIdHex(v.Id)).One(&product); err != nil {
			fmt.Fprint(w, "Error finding data Id doesnot exists")
			return
		}
		count, err := strconv.Atoi(v.Count)
		if err != nil {
			fmt.Fprint(w, "Error converting to Int")
			return
		}
		price := product.Price * float64(count)
		bill = models.Bill{
			product,
			v.Count,
			price,
		}
		sum = sum + bill.Price
		billsList = append(billsList, bill)
	}
	totalBill := models.TotalBill{
		billsList,
		sum,
	}
	tpl.ExecuteTemplate(w, "billing.gohtml", totalBill)
}

func (pc ProductController) Checkout(w http.ResponseWriter, r *http.Request) {
	totalPrice := r.FormValue("totalPrice")
	cookie, err := r.Cookie("session")
	if err != nil {
		fmt.Fprint(w, "No data exists in cookie")
		return
	}
	checkoutData := models.CheckoutData{
		bson.NewObjectId(),
		cookie.Value,
		totalPrice,
	}

	cookieValue, err := base64.StdEncoding.DecodeString(cookie.Value)
	if err != nil {
		fmt.Fprint(w, "Unable to decode to string")
		return
	}
	var cookieValueList []models.CookieModel
	json.Unmarshal(cookieValue, &cookieValueList)

	for _, v := range cookieValueList {
		product := models.Product{}
		if err := pc.session.DB(dbName).C(productCollectionName).FindId(bson.ObjectIdHex(v.Id)).One(&product); err != nil {
			fmt.Fprint(w, "Data Not Found")
			return
		}
		count, err := strconv.Atoi(v.Count)
		if err != nil {
			fmt.Fprint(w, "Error converting to Int")
			return
		}
		product.Quantity = product.Quantity - int64(count)
		if err := pc.session.DB(dbName).C(productCollectionName).UpdateId(product.Id, &product); err != nil {
			fmt.Fprint(w, "Unable to update data")
			return
		}
	}

	if err := pc.session.DB(dbName).C(billCollectionName).Insert(checkoutData); err != nil {
		fmt.Fprint(w, "Unable to Insert data")
		return
	}
	cookie.MaxAge = -1
	http.SetCookie(w, cookie)
	tpl.ExecuteTemplate(w, "checkout.gohtml", nil)
}

func (pc ProductController) GetAllMyBills(w http.ResponseWriter, r *http.Request) {
	var checkoutList []models.CheckoutData
	err := pc.session.DB(dbName).C(billCollectionName).Find(nil).All(&checkoutList)
	if err != nil {
		fmt.Fprint(w, "Data Not found")
		return
	}
	var allBills []models.AllBills

	for _, v := range checkoutList {

		cartData, _ := base64.StdEncoding.DecodeString(v.Products)
		var cookieModelList []models.CookieModel
		var cartList []models.Cart
		json.Unmarshal(cartData, &cookieModelList)

		for _, v := range cookieModelList {
			product := models.Product{}
			if err := pc.session.DB(dbName).C(productCollectionName).FindId(bson.ObjectIdHex(v.Id)).One(&product); err != nil {
				fmt.Fprint(w, "Data Not found")
				return
			}
			cart := models.Cart{
				product,
				v.Count,
			}
			cartList = append(cartList, cart)
		}
		allbill := models.AllBills{
			cartList,
			v.TotalPrice,
		}
		allBills = append(allBills, allbill)
	}

	tpl.ExecuteTemplate(w, "bills.gohtml", allBills)
}

func (pc ProductController) UpdateCartCookie(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		cookie := productRepo.GetCookie(w, r)
		id := r.FormValue("Id")
		count := r.FormValue("count")
		cookie = productRepo.AppendValue(w, cookie, productRepo.GetHexFromString(id), count)
	}

	http.Redirect(w, r, r.Header.Get("Referer"), 302)
}
