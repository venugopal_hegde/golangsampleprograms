package main

import "fmt"

var y = 42
var z = "Helllo"
var a = `James said,

"Shaken , not stirred"`

type hotdog int

var b hotdog

func main() {
	fmt.Println(y)
	fmt.Printf("%T\n", y)
	fmt.Println(z)
	fmt.Printf("%T\n", z)
	fmt.Println(a)
	b = 43
	fmt.Printf("%T\n", b)
	y = int(b)
	fmt.Println(y)
}
