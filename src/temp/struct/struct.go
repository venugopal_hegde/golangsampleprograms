package main

import "fmt"

type person struct {
	first string
	last  string
}

type secretAgent struct {
	person
	ltk bool
}

func main() {
	p1 := person{
		first: "james",
		last:  "bond",
	}
	p2 := person{
		first: "miss",
		last:  "moneyphoney",
	}
	fmt.Println(p1)
	fmt.Println(p2)
	fmt.Println(p1.first)
	fmt.Println(p1.last)

	sa1 := secretAgent{
		person: person{
			first: "venu",
			last:  "gopal",
		},
		ltk: true,
	}

	fmt.Println(sa1.person.first, sa1.person.last, sa1.ltk)

	y := 20
	z := 40
	if y == 20 && z == 40 {
		fmt.Println("haaaaiiiiii")
	}
}
