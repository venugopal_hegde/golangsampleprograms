package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	fmt.Println("Logging")
	_, error := os.Open("nofile.txt")
	if error != nil {
		fmt.Println("error happened: ", error)
		log.Println("error happened: ", error)
		log.Panic(error)
	}
}
