package main

import "fmt"

func main() {

	x := []int{4, 5, 7, 8, 42}
	fmt.Println(len(x))
	fmt.Println(x[0])
	for i, v := range x {
		fmt.Println(i, v)
	}
	fmt.Println(x[1:])
	fmt.Println(x[1:3])
	x = append(x, 77, 88, 99, 1014)
	fmt.Println(x)
	fmt.Println("-----------------------")
	y := []int{234, 456, 678, 999}
	x = append(x, y...)
	fmt.Println(x)
	fmt.Println("---------------------DELETING---------")
	x = append(x[:2], x[4:]...)
	fmt.Println(x)
}
