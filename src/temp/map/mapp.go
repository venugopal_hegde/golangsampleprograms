package main

import "fmt"

func main() {
	m := map[string]int{
		"james": 34,
		"Miss":  20,
	}
	fmt.Println(m)
	fmt.Println(m["james"])

	v, ok := m["goggogo"]
	fmt.Println(v)
	fmt.Println(ok)

	if v, ok := m["Miss"]; ok {
		fmt.Println("This is te IF print", v)
	}
	m["toddy"] = 67

	for k, v := range m {
		fmt.Println(k, v)
	}

	xi := []int{4, 5, 6, 8, 9}
	for i, v := range xi {
		fmt.Println(i, v)
	}

	// delete(m, "toddy")
	// fmt.Println(m)

	if v, ok := m["toddy"]; ok {
		fmt.Println("value:", v)
		delete(m, "toddy")
	}

}
