package main

import "fmt"

var i int

func main() {
	x := 10
	fmt.Println(x)
	x = 99
	fmt.Println(x)
	y := 100 + 24
	fmt.Println(y)
	z := "venugopal"
	fmt.Println(z)
}
