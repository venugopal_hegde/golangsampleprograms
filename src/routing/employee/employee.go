package employee

//Employee - Here we are exporting employee model
type Employee struct {
	ID          int
	name        string
	address     string
	designation string
	Rating      float64
}
