package employee

import (
	"io"
	"net/http"
)

//Handler function
func Handler(writer http.ResponseWriter, request *http.Request) {
	io.WriteString(writer, "In Employee handler")
}
